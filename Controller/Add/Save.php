<?php

namespace Bach\Internship\Controller\Add;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;

class Save extends Action
{
    protected $userFactory;

    public function __construct(
        Context $context,
        \Bach\Internship\Model\UsersFactory $usersFactory
    )
    {
        parent::__construct($context);
        $this->userFactory = $usersFactory;
    }

    public function execute()
    {
        $data = $this->_request->getParams();
        $this->userFactory->create()->setData($data)->save();
        return $this->_redirect('home');
    }
}
