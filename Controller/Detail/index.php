<?php


namespace Bach\Internship\Controller\Detail;

use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;

class Index extends Action
{
    protected $_userFactory;
    protected $_pageFactory;

    public function __construct(
        Context $context,
        \Bach\Internship\Model\UsersFactory $usersFactory,
        \Magento\Framework\View\Result\PageFactory $pageFactory
    ) {
        parent::__construct($context);
        $this->_pageFactory = $pageFactory;
        $this->_userFactory = $usersFactory;
    }

    public function execute()
    {
        return $this->_pageFactory->create();
    }
}