var config = {
    map: {
        "*": {
            // 'owl.carousel': "Bach_Internship/js/owl.carousel",
            'widgetDemo': "Bach_Internship/js/widget_carousel",
            'catalogAddToCart': "Bach_Internship/js/catalog-add-to-cart",
        }
    },
    shim: {
        'owl.carousel': {
            deps: ['jquery']
        },
        'widgetDemo': {
            deps: ['jquery']
        }
    }

};