define(
    [
        'jquery',
        'Bach_Internship/js/owl.carousel',
        'Bach_Internship/js/widget_carousel',
    ],
    function ($, _) {
        'use strict';
        $.widget(
            'internship.widgetDemo',
            {
                _create: function () {
                    $(this.element).owlCarousel(this.options);
                }
            }
        );
        return $.internship.widgetDemo;
    }
);

