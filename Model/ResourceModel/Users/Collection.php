<?php

namespace Bach\Internship\Model\ResourceModel\Users;

use Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection;

class Collection extends AbstractCollection
{
    /**
     * Custom method
     */
    protected function _construct()
    {
        $this->_init(
            'Bach\Internship\Model\Users',
            'Bach\Internship\Model\ResourceModel\Users'
        );
    }
}