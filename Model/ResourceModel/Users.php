<?php

namespace Bach\Internship\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;

class Users extends AbstractDb
{
    /**
     * Custom method
     */
    public function _construct()
    {
        $this->_init('internship', 'id');
    }
}