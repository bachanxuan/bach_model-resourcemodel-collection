<?php
/**
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Bach\Internship\Setup;

use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;

/**
 * Custom Class
 *
 * @codeCoverageIgnore
 */
class InstallData implements InstallDataInterface
{

        /**
         * {@inheritdoc}
         *
         * @SuppressWarnings(PHPMD.CyclomaticComplexity)
         * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
         * @SuppressWarnings(PHPMD.NPathComplexity)
         */
        public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
                $data = [
                        [
                                'name' => 'Nguyen Van A',
                                'avatar' => '#',
                                'dob' => '2020/01/01',
                                'description' => 'dep trai nhung gay'
                                ],
                        [
                                'name' => 'Nguyen thuy C',
                                'avatar' => '#',
                                'dob' => '1988/02/02',
                                'description' => 'xinh gai'
                                ],[
                                'name' => 'Le Hong Hao',
                                'avatar' => '#',
                                'dob' => '1990/08/10',
                                'description' => 'dasdasdada'
                                ],[
                                'name' => 'Doan Thi T',
                                'avatar' => '#',
                                'dob' => '2010/01/06',
                                'description' => 'orweqenqeq'
                                ],[
                                'name' => 'Nguyen Quang D',
                                'avatar' => '#',
                                'dob' => '1991/01/08',
                                'description' => 'cuc tinh'
                                ],
                    ];

                foreach ($data as $bind) {
                            $setup->getConnection()
                                ->insertForce($setup->getTable('internship'), $bind);
        }
    }
}