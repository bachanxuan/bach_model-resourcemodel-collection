<?php
/**
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Bach\Internship\Setup;

use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

/**
 * Custom Class
 *
 * @codeCoverageIgnore
 */
class InstallSchema implements InstallSchemaInterface
{
    /**
     * {@inheritdoc}
     *
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        /**
         * Create table 'internship'
         */
        $installer = $setup;
        $installer->startSetup();
        $connection = $installer->getConnection();

        if (!$installer->tableExists('internship')) {
            $table = $connection->newTable($installer->getTable('internship'))
                ->addColumn(
                    'id',
                    \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
                    null,
                    ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
                    'internship ID'
                )->addColumn(
                    'name',
                    Table::TYPE_TEXT,
                    null,
                    ['nullable' => true]
                )->addColumn(
                    'avatar',
                    Table::TYPE_TEXT,
                    null,
                    ['nullable' => true]
                )->addColumn(
                    'dob',
                    Table::TYPE_DATE,
                    null,
                    ['nullable' => true]

                )->addColumn(
                    'description',
                    Table::TYPE_TEXT,
                    null,
                    ['nullable' => true]
                )
                ->setComment('internship information');
            $connection->createTable($table);
        }
    }
}