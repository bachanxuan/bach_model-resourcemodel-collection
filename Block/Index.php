<?php

namespace Bach\Internship\Block;

use Magento\Framework\View\Element\Template;

class Index extends \Magento\Framework\View\Element\Template
{
    protected $_userFactory;

    public function __construct(
        Template\Context $context,
        $data = [],
        \Bach\Internship\Model\UsersFactory $usersFactory
    )
    {
        $this->_userFactory = $usersFactory;
        parent::__construct($context, $data);
    }

    public function getUserById()
    {

        if ($this->getRequest()->getParams('id')) {
            $id = $this->getRequest()->getParams('id');
            $userFactory = $this->_userFactory->create()->load($id);
            return $userFactory->getData();
        }

    }
}